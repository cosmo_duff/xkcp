# XKCP

## Summary

xkcp is a passphrase generator based off of [XKCD 936](https://xkcd.com/936/). This was mostly created to fulfill my needs and to write some Rust.
If you are looking for a more full featured and battle tested program check out [xkcdpass](https://github.com/redacted/XKCD-password-generator).

## Building/Installation

This program is written in Rust and can be compiled and installed using the standard Rust tool chain. You can find Rust installation information for your OS on [Rust's official site](https://www.rust-lang.org/). The only currently tested platform is Linux though it should work on others in theory. If you try it please let me know.

To build:

```
$ git clone https://gitlab.com/cosmo_duff/xkcp
$ cd xkcp
$ cargo build --release
```

To install:

```
$ git clone https://gitlab.com/cosmo_duff/xkcp
$ cd xkcp
$ cargo install --path ./
```

This will place a compiled binary in $HOME/.cargo/bin on Unix and Unix like systems. 

## Usage/Examples

Usage:

```
xkcp 0.3.0
Generates xkcd style passphrases (XKCD 936)

USAGE:
    xkcp [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
        --multiple-delimiters         If set multiple random delimiters will be used form the supplied delimiters
    -R, --random-special-character    Chooses a random special character for the delimiter. If set the value for
                                      delimiter is ignored
    -h, --help                        Prints help information
    -V, --version                     Prints version information

OPTIONS:
    -C, --capitalization <CAPITALIZATION>    Capitalization scheme. [default: lower] [possible values: upper, lower,
                                             capitalize, random]
    -c, --count <COUNT>                      Number of random words for the passphrase. [default: 4]
    -d, --delimiter <DELIMETER>...           Delimiter for the words in the passphrase. If multiple delimiters are given
                                             a random one will be chesen from the list.[default: " "]
    -M, --maximum-length <MAXIMUM LENGTH>    Maximum password length
    -m, --minimum-length <MINIMUM LENGTH>    Minimum password length
        --multiple <MULTIPLE>                Generate multiple different passphrases. [default: 1]
    -n, --number <NUMBER>                    Adds a random number to the passphrase with the set value as the upper
                                             limit. [max value: 18446744073709551615]
    -r, --retries <RETRIES>                  Number of times to retry generating a password. [default: 100]
    -w, --word-list <WORD LIST>              Which word list to use. [default: long] [possible values: short, long]

SUBCOMMANDS:
    help      Prints this message or the help of the given subcommand(s)
    policy


```

Examples:

No flags all default behavior
```
$ xkcp
guidable destruct dinner sneer

```

Setting a delimiter for the words
```
$ xkcp -d %
myspace%chosen%paced%floral

```

Set a delimiter and change the capitalization to all uppercase
```
$ xkcp -d % -C upper
UNDERMOST%UNHEALTHY%DIVIDED%CROOK

```

Set a delimiter change the capitalization to all uppercase and add a random number between 0 and 100
```
$ xkcp -d % -C upper -n 100
ANGUISHED%CUT%BRISKNESS%DEEPEN%86
```

Set a delimiter change the capitalization to upper and generate 4 different passwords
```
$ xkcp -d % -C upper --multiple 4
ANGELIC%MANHUNT%STRIVING%PARDON
STUCCO%UNDEFINED%SURELY%DIRECTION
OUTSIDER%PLATYPUS%SNOWBOARD%DYNAMITE
DOORNAIL%ONION%GEOLOGIC%ENDANGER

```


Set a delimiter change the capitalization to upper and generate 4 different passwords with a maximum length of 12 and only use 2 random words.
```
$ xkcp -d % -C upper --multiple 4 -M 12 -c 2
THAT%RUBBLE
PLATTER%STEW
DUCT%AGROUND
AEROSOL%OPAL

```

Generate 5 passwords each using a random special character as a delimiter.
```
$ xkcp -R --multiple 5
sculpture}gyration}taco}variably
harmony]yen]cannabis]thank
humorous^vintage^hypnotist^expectant
fried<symphony<pushup<arrest
conjuror|challenge|cupbearer|collision

```

Supply several possible delimiters and a random one will be chosen.
```
$ xkcp -d ":" "-" ")" --multiple 5
fossil)overpass)jester)unaligned
amount)banana)dimly)faculty
shock)dullness)stammer)royal
unmolded:antennae:docile:duplicate
unafraid:unbounded:barcode:submerge

```

Use multiple different random delimiters to join the words.
```
$ xkcp -R --multiple-delimiters --multiple 5
stinking-dealer)scabby)uncover
gerbil-trickery-sliced.slang
duchess/cabbage!value?directed
afflicted]repose!onset.creed
sludge`shine=grime.plexiglas

```

## Password Policies

Password policies can be defined at $HOME/.config/xkcp/policies.toml. Policies.toml is a toml file that can contain named sections that define the options you would like xkcp to use. If no argument is passed to policy it will try and load a policy named default.

Usage:

```
xkcp-policy 0.3.0

USAGE:
    xkcp policy [POLICY]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <POLICY>    Name of a password policy to load from $HOME/.config/xkcp/policies.toml [default: default]

```

Example policies.toml:

```
[test]
capitalization = "capitalize"
delimiter = ["#"]
multiple = 4
count = 4
number = 1000
```

Example output using the example policies.toml:
```
$ xkcp policy test
Plus#Trilogy#Hardcover#Zipfile#33
External#Mandatory#Sabbath#Wisdom#660
Outclass#National#Usual#Irritate#843
Clinking#Yeah#Starlet#Tranquil#56

```

Command line flags passed when using policy will overwrite the settings in the policy:
```
$ xkcp -c 5 -n 10000 --multiple 2 policy test
Sleeve#Striving#Sandlot#Dainty#Ether#665
Cold#Collapse#Unbroken#Uncaring#Affirm#8116

```

## xkcp-core

The password generation is handled by the library xkcp-core which can be found in this repo [here](./xkcp-core).
