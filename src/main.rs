mod args;
mod error;
mod lists;

use std::io::{stderr, stdout, Write};

use crate::args::{read_policy, Args, Subcommand};
use crate::error::XkcpError;
use crate::lists::List;

use rand::thread_rng;
use structopt::StructOpt;
use xkcp_core::{GeneratePassword, PasswordGenerator};

fn main() {
    let args = Args::from_args();
    if let Err(e) = run(args) {
        eprintln!("{}", e);
        std::process::exit(1);
    };
}

fn run(args: Args) -> Result<(), XkcpError> {
    // try and get the policy from the policy file if set
    // if the policy is not found an error is returned
    let policy_args: Option<Args> = if let Some(ref p) = args.policy {
        let policy_name = match p {
            Subcommand::Policy { policy: pol } => pol.to_owned(),
        };
        let policies = read_policy()?;
        if let Some(policy) = policies.get(&policy_name) {
            Some(policy.clone())
        } else {
            return Err(XkcpError::PolicyNotFound(policy_name));
        }
    } else {
        None
    };
    // merge the cli args into the policy file args if set
    let args = if let Some(mut pa) = policy_args {
        pa.merge_args(&args);
        pa
    } else {
        args
    };
    // set defaults for some values
    let multiple = if let Some(m) = args.multiple { m } else { 1 };
    let retries = if let Some(r) = args.retries { r } else { 100 };
    let min_len = if let Some(min) = args.min_len { min } else { 1 };
    let max_len = if let Some(max) = args.max_len {
        max
    } else {
        std::usize::MAX
    };
    let mut rng = thread_rng();
    let password: PasswordGenerator<List> = args.into();
    let passwords =
        password.try_generate_passwords(&mut rng, multiple, retries, Some(min_len), Some(max_len));
    let stdout = stdout();
    let stderr = stderr();
    let mut stdout_handle = stdout.lock();
    let mut stderr_handle = stderr.lock();
    for pass in passwords {
        if let Some(p) = pass {
            stdout_handle.write_fmt(format_args!("{}\n", p))?;
        } else {
            stderr_handle.write_fmt(format_args!(
                "Could not generate a password between {} and {} in length\n",
                min_len, max_len
            ))?;
        }
    }
    stdout_handle.flush()?;
    stderr_handle.flush()?;
    Ok(())
}
