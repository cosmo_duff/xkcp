use std::convert::From;
use std::env::VarError;
use std::error::Error;
use std::fmt;
use std::fmt::Display;

#[derive(Debug)]
pub enum XkcpError {
    NoHome(VarError),
    PolicyFileDoesNotExist,
    Io(std::io::Error),
    PolicyDeserializeFailed(toml::de::Error),
    PolicyNotFound(String),
}

impl Display for XkcpError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            XkcpError::NoHome(e) => write!(
                f,
                "Could not determine the home directory to load the policies: {}.",
                e
            ),
            XkcpError::PolicyFileDoesNotExist => write!(
                f,
                "The policy file $HOME/.config/xkcp/policies.toml does not exist."
            ),
            XkcpError::Io(e) => write!(f, "{}", e),
            XkcpError::PolicyDeserializeFailed(e) => {
                write!(f, "Failed to parse the policy file: {}.", e)
            }
            XkcpError::PolicyNotFound(p) => {
                write!(f, "Failed to find a policy named {} in the policy file.", p)
            }
        }
    }
}

impl Error for XkcpError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match *self {
            XkcpError::NoHome(ref e) => Some(e),
            XkcpError::Io(ref io) => Some(io),
            _ => None,
        }
    }
}

impl From<std::io::Error> for XkcpError {
    fn from(err: std::io::Error) -> Self {
        XkcpError::Io(err)
    }
}

impl From<std::env::VarError> for XkcpError {
    fn from(err: std::env::VarError) -> Self {
        XkcpError::NoHome(err)
    }
}
