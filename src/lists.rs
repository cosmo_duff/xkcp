use dicewords::EffWordList;
use serde::Deserialize;
use xkcp_core::WordList;
//use std::path::{Path, PathBuf};
//use std::str::FromStr;

#[derive(Clone, Debug, Deserialize)]
pub enum List {
    Custom(Vec<String>),
    Eff(EffWordList),
}

impl WordList for List {
    fn get_random_words<R: rand::Rng>(&self, rng: &mut R, count: usize) -> Vec<String> {
        match self {
            List::Eff(e) => e.get_random_words(rng, count),
            List::Custom(l) => l.get_random_words(rng, count),
        }
    }
}

//impl FromStr for List {
//    // This cant really error since the Custom variant is a catch all
//    type Err = &'static str;
//
//    fn from_str(s: &str) -> Result<Self, Self::Err> {
//        match s {
//            "short" => Ok(List::Eff(EffWordList::Short)),
//            "long" => Ok(List::Eff(EffWordList::Long)),
//            _ => Ok(List::Custom(Path::new(s).to_path_buf())),
//        }
//    }
//}
