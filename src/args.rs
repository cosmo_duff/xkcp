use std::{
    collections::HashMap,
    env,
    fs::File,
    io,
    io::{prelude::*, BufReader},
    path::{Path, PathBuf},
};

use crate::{error::XkcpError, lists::List};

use serde::Deserialize;
use structopt::clap::AppSettings;
use structopt::StructOpt;
use xkcp_core::prelude::*;

#[derive(Debug, Clone, StructOpt, Deserialize)]
#[serde(default)]
#[structopt(name = "xkcp", about = "Generates xkcd style passphrases (XKCD 936)")]
pub struct Args {
    /// Number of random words for the passphrase. [default: 4]
    #[structopt(name = "COUNT", short = "c", long = "count")]
    pub count: Option<usize>,
    /// Delimiter for the words in the passphrase. If multiple delimiters are given a random one
    /// will be chosen from the list. [default: " "]
    #[structopt(
        name = "DELIMETER",
        short = "d",
        long = "delimiter",
        number_of_values = 1
    )]
    pub delimiter: Option<Vec<String>>,
    /// If set multiple random delimiters will be used from the supplied delimiters.
    #[structopt(name = "MULTIPLE DELIMITERS", long = "multiple-delimiters")]
    pub multiple_delimiters: bool,
    ///Chooses a random special character for the delimiter. If set the value for delimiter is
    ///ignored.
    #[structopt(
        name = "RANDOM SPECIAL CHARACTER",
        short = "R",
        long = "random-special-character"
    )]
    pub random_special_character: bool,
    /// Adds a random number to the passphrase with the set value as the upper limit.
    /// [max value: 18446744073709551615]
    #[structopt(name = "NUMBER", short = "n", long = "number")]
    pub number: Option<u64>,
    /// Places the random number randomly in the passphrase
    ///
    /// Only has an effect if number is set
    #[structopt(
        name = "RANDOM NUMBER PLACEMENT",
        short = "N",
        long = "random-number-placement"
    )]
    pub random_number_placement: bool,
    /// Generate multiple different passphrases. [default: 1]
    #[structopt(name = "MULTIPLE", long = "multiple")]
    pub multiple: Option<usize>,
    /// Maximum password length.
    // The default val is the maximum for a u64
    #[structopt(name = "MAXIMUM LENGTH", short = "M", long = "maximum-length")]
    pub max_len: Option<usize>,
    /// Minimum password length.
    #[structopt(name = "MINIMUM LENGTH", short = "m", long = "minimum-length")]
    pub min_len: Option<usize>,
    /// Number of times to retry generating a password. [default: 100]
    #[structopt(name = "RETRIES", short = "r", long = "retries")]
    pub retries: Option<u64>,
    /// Capitalization scheme. [default: lower]
    #[structopt(
        name = "CAPITALIZATION",
        short = "C",
        long = "capitalization",
        possible_values = &["upper", "lower", "capitalize", "random"]
    )]
    pub capitalization: Option<Capitalization>,
    /// Which word list to use. If set to short of long the short or long EFF wordlists
    /// will be used. If set to a path a custom wordlist will be loaded. [default: EFF long]
    #[structopt(name = "WORD LIST", short = "w", long = "word-list", parse(try_from_str = parse_list))]
    pub word_list: Option<List>,
    /// Load settings from a policy file
    #[structopt(subcommand)]
    pub policy: Option<Subcommand>,
}

#[derive(Debug, Clone, StructOpt, Deserialize)]
#[structopt(setting = AppSettings::InferSubcommands)]
pub enum Subcommand {
    Policy {
        /// Name of a password policy to load from $HOME/.config/xkcp/policies.toml
        #[structopt(name = "POLICY", default_value = "default")]
        policy: String,
    },
}

impl Default for Args {
    fn default() -> Self {
        Args {
            count: None,
            delimiter: None,
            multiple_delimiters: false,
            random_special_character: false,
            number: None,
            random_number_placement: false,
            multiple: None,
            capitalization: None,
            max_len: None,
            min_len: None,
            policy: None,
            retries: None,
            word_list: None,
        }
    }
}

fn parse_list(list: &str) -> io::Result<List> {
    match list {
        "short" => Ok(List::Eff(EffWordList::ShortWord1)),
        "long" => Ok(List::Eff(EffWordList::LongWord)),
        path => {
            let list = read_list(Path::new(path))?;
            Ok(List::Custom(list))
        }
    }
}

impl Args {
    pub fn merge_args(&mut self, args: &Args) {
        if args.random_special_character {
            self.random_special_character = args.random_special_character;
        }
        if args.multiple_delimiters {
            self.multiple_delimiters = args.multiple_delimiters;
        }
        if args.count.is_some() {
            self.count = args.count;
        }
        if args.delimiter.is_some() {
            self.delimiter = args.delimiter.clone();
        }
        if args.number.is_some() {
            self.number = args.number;
        }
        if args.random_number_placement {
            self.random_number_placement = args.random_number_placement
        }
        if args.multiple.is_some() {
            self.multiple = args.multiple;
        }
        if args.capitalization.is_some() {
            self.capitalization = args.capitalization.clone();
        }
        if args.max_len.is_some() {
            self.max_len = args.max_len;
        }
        if args.min_len.is_some() {
            self.min_len = args.min_len;
        }
        if args.retries.is_some() {
            self.retries = args.retries;
        }
        if args.word_list.is_some() {
            self.word_list = args.word_list.clone();
        }
    }
}

impl From<Args> for PasswordGenerator<List> {
    fn from(args: Args) -> Self {
        let mut generator = if let Some(w) = args.word_list {
            PasswordGenerator::builder(w)
        } else {
            PasswordGenerator::builder(List::Eff(EffWordList::LongWord))
        };

        if let Some(c) = args.count {
            generator.count(c);
        }
        // if random special character is set the other values are ignored.
        if args.random_special_character {
            generator.delimiter(Delimiter::RandomSpecialCharacter);
        } else if let Some(d) = args.delimiter {
            generator.delimiter(Delimiter::from(d));
        }
        if args.multiple_delimiters {
            generator.multiple_delimiters(args.multiple_delimiters);
        }
        if let Some(n) = args.number {
            generator.number(n);
        }
        if let Some(c) = args.capitalization {
            generator.capitalization(c);
        }
        generator.random_number_placement(args.random_number_placement);

        generator.build()
    }
}

pub(crate) fn read_list(path: &Path) -> io::Result<Vec<String>> {
    BufReader::new(File::open(path)?).lines().collect()
}

pub(crate) fn read_policy() -> Result<HashMap<String, Args>, XkcpError> {
    let home_path = env::var("HOME")?;
    let mut policies_path = PathBuf::from(home_path);
    policies_path.push(".config/xkcp/policies.toml");
    if !policies_path.exists() {
        return Err(XkcpError::PolicyFileDoesNotExist);
    }
    let f = File::open(&policies_path)?;
    let mut reader = BufReader::new(f);
    let mut conf_str = String::new();
    reader.read_to_string(&mut conf_str)?;
    let policies: Result<HashMap<String, Args>, toml::de::Error> = toml::from_str(&conf_str);
    match policies {
        Ok(p) => Ok(p),
        Err(e) => Err(XkcpError::PolicyDeserializeFailed(e)),
    }
}
