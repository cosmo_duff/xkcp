// Create static word list
use std::{
    env,
    fs::{write, File},
    io::{prelude::*, BufReader},
    path::{Path, PathBuf},
};

fn main() {
    println!("cargo:rerun-if-changed=word_lists/eff_large_wordlist.txt");
    println!("cargo:rerun-if-changed=word_lists/eff_short_wordlist_1.txt");
    let lrg_list = PathBuf::from("word_lists/eff_large_wordlist.txt");
    let short_list1 = PathBuf::from("word_lists/eff_short_wordlist_1.txt");
    let short_list2 = PathBuf::from("word_lists/eff_short_wordlist_2.txt");
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let lrg_dest = Path::new(&out_dir).join("large_word_list.rs");
    let short_dest1 = Path::new(&out_dir).join("short_word_list1.rs");
    let short_dest2 = Path::new(&out_dir).join("short_word_list2.rs");
    // Panicing is fine since we want the build to fail if this does not work
    //create_list(&lrg_list, &lrg_dest, "LONG_WORD_LIST").unwrap();
    //create_list(&short_list, &short_dest, "SHORT_WORD_LIST").unwrap();
    create_phf_list(&lrg_list, &lrg_dest, "LONG_WORD_LIST").unwrap();
    create_phf_list(&short_list1, &short_dest1, "SHORT_WORD_LIST1").unwrap();
    create_phf_list(&short_list2, &short_dest2, "SHORT_WORD_LIST2").unwrap();
}

//fn create_list(src: &Path, dest: &Path, var_name: &str) -> std::io::Result<()> {
//    let f = File::open(src)?;
//    let buf_reader = BufReader::new(f);
//    let mut output = String::new();
//    output.push_str(&format!(
//        "lazy_static! {{\n\tstatic ref {}: HashMap<u32, &'static str> = {{\n\t\tlet m = HashMap::from([\n",
//        var_name
//    ));
//
//    for line in buf_reader.lines() {
//        if let Ok(l) = line {
//            let mut iter = l.split_whitespace();
//            let num = if let Some(n) = iter.next() {
//                n
//            } else {
//                continue;
//            };
//            let word = if let Some(w) = iter.next() {
//                w
//            } else {
//                continue;
//            };
//            output.push_str(&format!("\t\t\t({}, \"{}\"),\n", num, word));
//        } else {
//            continue;
//        }
//    }
//
//    output.push_str("\t\t]);\n\t\tm\n\t};\n}");
//
//    write(dest, &output).expect("failed to write the word list");
//
//    Ok(())
//}

fn create_phf_list(src: &Path, dest: &Path, var_name: &str) -> std::io::Result<()> {
    let f = File::open(src)?;
    let buf_reader = BufReader::new(f);
    let mut output = String::new();
    let mut phf_gen = phf_codegen::Map::new();

    let mut entries: Vec<(u32, String)> = Vec::new();
    for line in buf_reader.lines() {
        if let Ok(l) = line {
            let mut iter = l.split_whitespace();
            let num = if let Some(n) = iter.next() {
                if let Ok(num) = n.parse::<u32>() {
                    num
                } else {
                    continue;
                }
            } else {
                continue;
            };
            let word = if let Some(w) = iter.next() {
                format!("\"{}\"", w)
            } else {
                continue;
            };
            entries.push((num, word));
        } else {
            continue;
        }
    }

    for entry in entries {
        phf_gen.entry(entry.0, &entry.1);
    }

    output.push_str(&format!(
        "static {}: phf::Map<u32, &'static str> = {}",
        var_name,
        phf_gen.build()
    ));

    output.push_str(";\n");

    write(dest, &output).expect("failed to write the word list");

    Ok(())
}
