include!(concat!(env!("OUT_DIR"), "/large_word_list.rs"));
include!(concat!(env!("OUT_DIR"), "/short_word_list1.rs"));
include!(concat!(env!("OUT_DIR"), "/short_word_list2.rs"));

use rand::Rng;
#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};
use std::num::ParseIntError;
use std::str::FromStr;

/// "Rolls" a dice using the provided Rng
pub fn roll_dice(dice: usize, rng: &mut impl Rng) -> Result<u32, ParseIntError> {
    let nums: Vec<String> = (0..dice)
        .map(|_| (rng.gen_range(0..5) + 1).to_string())
        .collect();
    // Should only really fail if the number of dice is longer that the possible places in usize
    // max
    nums.join("").parse::<u32>()
}

/// Represents the possible EFF word lists
#[derive(Clone, Debug)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
pub enum EffWordList {
    LongWord,
    ShortWord1,
    ShortWord2,
}

impl Default for EffWordList {
    fn default() -> Self {
        EffWordList::LongWord
    }
}

impl FromStr for EffWordList {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lower = s.to_lowercase();
        match lower.as_str() {
            "short1" => Ok(EffWordList::ShortWord1),
            "short2" => Ok(EffWordList::ShortWord2),
            "long" => Ok(EffWordList::LongWord),
            _ => Err("Unknown EFF word list."),
        }
    }
}

impl EffWordList {
    /// Gets the number of dice that should be rolled
    pub fn get_dice_count(&self) -> usize {
        match *self {
            EffWordList::LongWord => 5,
            EffWordList::ShortWord1 => 4,
            EffWordList::ShortWord2 => 4,
        }
    }

    /// Chooses "rolls" dice to choose a word from the list
    pub fn choose_word(&self, rng: &mut impl Rng) -> String {
        // the largest number that can be created from EffWordList is 666666 which is smaller than
        // usize
        let dice_nums = roll_dice(self.get_dice_count(), rng).unwrap();
        let list = self.choose_list();
        // with the dice numbers being limited we can confidently unwrap since we should always get
        // a return
        // if this turns out not to be the case I will address this later
        list.get(&dice_nums).unwrap().to_string()
    }

    /// Chooses mulitple words from the word list
    pub fn choose_words(&self, count: usize, mut rng: &mut impl Rng) -> Vec<impl Into<String>> {
        (0..count).map(|_| self.choose_word(&mut rng)).collect()
    }

    fn choose_list(&self) -> &phf::Map<u32, &'static str> {
        match *self {
            EffWordList::LongWord => &LONG_WORD_LIST,
            EffWordList::ShortWord1 => &SHORT_WORD_LIST1,
            EffWordList::ShortWord2 => &SHORT_WORD_LIST2,
        }
    }
}

trait DiceWordList {
    fn get_dice_count(&self) -> usize;
    fn choose_word(&self, rng: &mut impl Rng) -> String;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic_word_get() {
        let result = LONG_WORD_LIST.get(&11111u32);
        assert_eq!(result, Some(&"abacus"));
    }

    #[test]
    fn test_dice_roll() {
        let mut rng = rand::thread_rng();
        let dice_num = roll_dice(5, &mut rng);
        assert!(dice_num.is_ok());
        assert_eq!(5, dice_num.unwrap().to_string().len());
        let dice_num_fail = roll_dice(15, &mut rng);
        assert!(dice_num_fail.is_err());
    }

    #[test]
    fn test_generating_word() {
        let mut rng = rand::thread_rng();
        // generate 100 words from each list
        let long_list = EffWordList::LongWord;
        for _ in 0..100 {
            long_list.choose_word(&mut rng);
        }
        let short_list1 = EffWordList::ShortWord1;
        for _ in 0..100 {
            short_list1.choose_word(&mut rng);
        }
        let short_list2 = EffWordList::ShortWord2;
        for _ in 0..100 {
            short_list2.choose_word(&mut rng);
        }
    }

    #[test]
    fn test_generating_words() {
        let mut rng = rand::thread_rng();
        let long_list = EffWordList::LongWord;
        for i in 1..101 {
            let words = long_list.choose_words(i, &mut rng);
            assert_eq!(i, words.len());
        }
        let short_list1 = EffWordList::ShortWord1;
        for i in 1..101 {
            let words = short_list1.choose_words(i, &mut rng);
            assert_eq!(i, words.len());
        }
        let short_list2 = EffWordList::ShortWord2;
        for i in 1..101 {
            let words = short_list2.choose_words(i, &mut rng);
            assert_eq!(i, words.len());
        }
    }
}
