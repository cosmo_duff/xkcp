# xkcp-core

xkcp-core is a library that generates passwords based off of [XKCD 936](https://xkcd.com/936/).

### Usage

Currently this crate is not available on [crates.io](https://crates.io).  It can be used by adding
`xkcp-core` to the projects `Cargo.toml` and specifying the git repo.
```toml
[dependencies]
xkcp-core = { git = "https://gitlab.com/cosmo_duff/xkcp.git" }
```

### Documentation

Documentation can be built using the `cargo doc` command.

```bash
$ git clone https://gitlab.com/cosmo_duff/xkcp.git && cd xkcp
$ cargo doc
# Open the doc in a web browser
$ cargo doc --open
```

### Example
```rust
// Imports:
//   GeneratorBuilder
//   PasswordGenerator
//   WordList
//   Capitalization
use xkcp_core::prelude::*;
use rand::thread_rng;

// Create a ThreadRng to perform the random selections. All functions to generate a password take
// a mutable object that implements the Rng trait.
let mut rng = thread_rng();

// Create the password generator object using the builder.
let password_generator = GeneratorBuilder::new()
    // Passwords will contain 5 random words.
    .count(5)
    // The words will be joined using a %.
    .delimiter(Delimiter::from("%"))
    // A random number from 0 to 1000 will be added to the password.
    .number(1000)
    // All characters will be capitalized.
    .capitalization(Capitalization::Capitalize)
    // Use the long word list for selecting the words.
    .word_list(WordList::Long)
    .build();

// This function can not fail and will generate a single password using the parameters set in the
// builder.
let password = password_generator.generate_password(&mut rng);

// This function can not fail and will return a vec of 5 random passwords.
let passwords = password_generator.generate_passwords(&mut rng, 5);

// The failable version of the generate password function. It will attempt to generate a password
// within the set minimum and maximum lengths with the set number or retries. If a password cannot
// be generated within the constraints None is returned. The example below will try 100 times to
// generate a password with a maximum length of 16.
let try_password = password_generator.try_generate_password(&mut rng, 100, None, Some(16));

// The failable version of generate_passwords. It works as try_generate_password but will return a
// vec of Option<String>. Each time a password cannot be generated within the constraints None is
// returned. The example will try to generate 10 passwords with a maximum length of 20 and can
// retry each password 100 times.
let try_passwords = password_generator.try_generate_passwords(&mut rng, 10, 100, None, Some(20));
```


### Word Lists

The word lists come from the EFF and can be found
[here](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases). The lists are
licensed under the [CC (Creative Commons) 3.0](https://creativecommons.org/licenses/by/3.0/us/).
The library uses the [long](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt) and
[short](https://www.eff.org/files/2016/09/08/eff_short_wordlist_1.txt) lists.
