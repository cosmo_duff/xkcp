use rand::Rng;
use std::fmt::Display;

use crate::capitalization::Capitalization;

/// A trait that represents a password generation.
pub trait GeneratePassword {
    /// Generates a password.
    fn generate_password<R: Rng>(&self, rng: &mut R) -> String;

    ///Generates multiple passwords.
    fn generate_passwords<R: Rng>(&self, rng: &mut R, count: usize) -> Vec<String>;

    /// Tries to generate a password with the set minimum and maximum length within the set number
    /// of retires.
    /// If a password cannot be generated with the specified parameters None is returned.
    fn try_generate_password<R: Rng>(
        &self,
        rng: &mut R,
        retries: u64,
        minimum_length: Option<usize>,
        maximum_length: Option<usize>,
    ) -> Option<String>;

    /// Tries to generate multiple passwords with the set minimum and maximum length within the set
    /// number of retires.
    ///
    /// If a password cannot be generated with the specified parameters None is returned.
    fn try_generate_passwords<R: Rng>(
        &self,
        rng: &mut R,
        count: usize,
        retries: u64,
        minimum_length: Option<usize>,
        maximum_length: Option<usize>,
    ) -> Vec<Option<String>>;
}
