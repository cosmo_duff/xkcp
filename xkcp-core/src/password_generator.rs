use crate::{Capitalization, Delimiter, GeneratePassword, GeneratorBuilder, WordList};

#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};

use rand::Rng;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
/// A password generator.
///
/// A PasswordGenerator can be created by either using a GeneratorBuilder or the new function.
/// The generator has both fallible and infallible functions for generating passwords. The password
/// generating functions are defined in the [`GeneratePassword`] trait.
pub struct PasswordGenerator<W> {
    /// Number of random words for the passphrase.
    pub(crate) count: usize,
    /// Delimiter for the words in the passphrase.
    pub(crate) delimiter: Delimiter,
    /// Uses multiple random delimiters.
    pub(crate) multiple_delimiters: bool,
    /// Adds a random number to the passphrase with the set value as the upper limit.
    pub(crate) number: Option<u64>,
    /// Places the random number in a random location in the string.
    ///
    /// Only has an effect if number is set.
    pub(crate) random_number_placement: bool,
    /// Capitalization scheme.
    pub(crate) capitalization: Capitalization,
    /// Which word list to use.
    pub(crate) word_list: W,
}

impl<W: WordList + Clone> PasswordGenerator<W> {
    /// Creates a new password generator.
    pub fn new(
        count: usize,
        delimiter: Delimiter,
        multiple_delimiters: bool,
        number: Option<u64>,
        random_number_placement: bool,
        capitalization: Capitalization,
        word_list: W,
    ) -> PasswordGenerator<W> {
        PasswordGenerator {
            count,
            delimiter,
            multiple_delimiters,
            number,
            random_number_placement,
            capitalization,
            word_list,
        }
    }

    /// Creates a builder for PasswordGenerator.
    pub fn builder(word_list: W) -> GeneratorBuilder<W> {
        GeneratorBuilder::new(word_list)
    }
}

impl<W: WordList> GeneratePassword for PasswordGenerator<W> {
    /// Generates a password.
    fn generate_password<R: Rng>(&self, rng: &mut R) -> String {
        let mut random_words: Vec<String> = self.word_list.get_random_words(rng, self.count);

        let number_pos = if self.number.is_some() && self.random_number_placement {
            rng.gen_range(0..=random_words.len())
        } else {
            random_words.len()
        };
        if let Some(n) = self.number {
            let num: u64 = rng.gen_range(0..=n);
            random_words.insert(number_pos, num.to_string());
        }
        for word in random_words.iter_mut() {
            self.capitalization.apply_scheme(word);
        }

        if self.multiple_delimiters {
            match self.delimiter {
                Delimiter::Random(_) | Delimiter::RandomSpecialCharacter => {
                    return multi_delimiter(rng, &random_words, &self.delimiter);
                }
                // if there is only one delimiter we do not need to do the multi join
                Delimiter::Character(_) => {}
            }
        }
        random_words.join(&self.delimiter.to_string(rng))
    }

    ///Generates multiple passwords.
    fn generate_passwords<R: Rng>(&self, rng: &mut R, count: usize) -> Vec<String> {
        (0..count)
            .into_iter()
            .map(|_| self.generate_password(rng))
            .collect()
    }

    /// Tries to generate a password with the set minimum and maximum length within the set number of
    /// retires.
    /// If a password cannot be generated with the specified parameters None is returned.
    fn try_generate_password<R: Rng>(
        &self,
        rng: &mut R,
        retries: u64,
        minimum_length: Option<usize>,
        maximum_length: Option<usize>,
    ) -> Option<String> {
        let min_len = minimum_length.unwrap_or(1);
        let max_len = maximum_length.unwrap_or(std::usize::MAX);

        for _ in 0..=retries {
            let password = self.generate_password(rng);
            if password.len() < min_len || password.len() > max_len {
                continue;
            } else {
                return Some(password);
            }
        }

        None
    }

    /// Tries to generate multiple passwords with the set minimum and maximum length within the set
    /// number of retires.
    ///
    /// If a password cannot be generated with the specified parameters None is returned.
    fn try_generate_passwords<R: Rng>(
        &self,
        rng: &mut R,
        count: usize,
        retries: u64,
        minimum_length: Option<usize>,
        maximum_length: Option<usize>,
    ) -> Vec<Option<String>> {
        (0..count)
            .into_iter()
            .map(|_| self.try_generate_password(rng, retries, minimum_length, maximum_length))
            .collect()
    }
}

/// Joins random words using a random delimiter between each word.
fn multi_delimiter<R: Rng>(rng: &mut R, words: &[String], delimiter: &Delimiter) -> String {
    // preallocate a string that should be more than big enough
    let mut output = String::with_capacity(100);
    let mut iter = words.iter().peekable();
    while let Some(word) = iter.next() {
        output.push_str(word);
        // if there is still another word in the list add a random delimiter
        if iter.peek().is_some() {
            output.push_str(&delimiter.to_string(rng));
        }
    }

    output
}
