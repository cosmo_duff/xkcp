use crate::prelude::*;

/// A configurable builder for a password generator.
///
/// The builder can be used to configure and create a password generator that can then be used to
/// generate random passwords.
pub struct GeneratorBuilder<W> {
    /// Number of random words for the passphrase.
    count: Option<usize>,
    /// Delimiter for the words in the passphrase.
    delimiter: Option<Delimiter>,
    /// Use multiple random delimiters.
    multiple_delimiters: Option<bool>,
    /// Adds a random number to the passphrase with the set value as the upper limit.
    number: Option<u64>,
    /// Places the random number in a random location with int he string.
    ///
    /// Only has an effect if number is set.
    random_number_placement: Option<bool>,
    /// Capitalization scheme.
    capitalization: Option<Capitalization>,
    /// Which word list to use.
    word_list: W,
}

impl<W> GeneratorBuilder<W>
where
    W: WordList + Clone,
{
    /// Creates a password generator builder.
    ///
    /// An object that implements WordList is the only required value. This library provides 2 word
    /// lists created by the EFF (See [`EffWordList`]).
    pub fn new(word_list: W) -> GeneratorBuilder<W> {
        GeneratorBuilder {
            count: None,
            delimiter: None,
            multiple_delimiters: None,
            number: None,
            random_number_placement: None,
            capitalization: None,
            word_list,
        }
    }

    /// Sets the number of words in the generated passphrase.
    pub fn count(&mut self, count: usize) -> &mut GeneratorBuilder<W> {
        self.count = Some(count);
        self
    }

    /// Sets the delimiter for the generated password.
    pub fn delimiter(&mut self, delimiter: Delimiter) -> &mut GeneratorBuilder<W> {
        self.delimiter = Some(delimiter);
        self
    }

    /// If true the words are joined by multiple random delimiters.
    pub fn multiple_delimiters(&mut self, multiple_delimiters: bool) -> &mut GeneratorBuilder<W> {
        self.multiple_delimiters = Some(multiple_delimiters);
        self
    }

    /// If set a random number from 0 to the set number will be added to the passphrase.
    pub fn number(&mut self, number: u64) -> &mut GeneratorBuilder<W> {
        self.number = Some(number);
        self
    }

    /// If set the random number will be placed in a random location within the passphrase.
    ///
    /// Only has an effect if number is also set.
    pub fn random_number_placement(
        &mut self,
        random_number_placement: bool,
    ) -> &mut GeneratorBuilder<W> {
        self.random_number_placement = Some(random_number_placement);
        self
    }

    /// Sets the capitalization scheme for the passphrase. Can be either upper, lower or capitalize.
    ///
    /// The scheme capitalize capitalizes the first letter in each word.
    pub fn capitalization(&mut self, capitalization: Capitalization) -> &mut GeneratorBuilder<W> {
        self.capitalization = Some(capitalization);
        self
    }

    ///Creates a PasswordGenerator from the builder.
    pub fn build(&mut self) -> PasswordGenerator<W> {
        // already an option so no coercion is needed
        let number = self.number;

        let random_number_placement = if let Some(rnp) = self.random_number_placement {
            rnp
        } else {
            false
        };

        let multiple_delimiters = if let Some(md) = self.multiple_delimiters {
            md
        } else {
            false
        };
        let count = if let Some(c) = self.count { c } else { 4 };
        let delimiter = if let Some(d) = &self.delimiter {
            d.clone()
        } else {
            Delimiter::default()
        };
        let capitalization = if let Some(cap) = &self.capitalization {
            cap.clone()
        } else {
            Capitalization::Lower
        };

        PasswordGenerator::new(
            count,
            delimiter,
            multiple_delimiters,
            number,
            random_number_placement,
            capitalization,
            self.word_list.clone(),
        )
    }
}
