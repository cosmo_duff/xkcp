use std::string::ToString;

use rand::{seq::IteratorRandom, Rng};
#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};

#[derive(Debug, Clone, Eq, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
/// The delimiter used in the password.
pub enum Delimiter {
    /// Any delimiter that can be represented as a String.
    Character(String),
    /// Chooses a random delimiter from the supplied list when the password is generated.
    Random(Vec<String>),
    /// Chooses a random special character when the password is generated.
    ///
    /// List of special chars: "!", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".",
    /// "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~",
    RandomSpecialCharacter,
}

impl Default for Delimiter {
    fn default() -> Self {
        Delimiter::Character(" ".to_string())
    }
}

impl From<Vec<String>> for Delimiter {
    fn from(delimiter_list: Vec<String>) -> Self {
        match delimiter_list.len() {
            0 => Delimiter::default(),
            1 => {
                if let Some(f) = delimiter_list.first() {
                    Delimiter::Character(f.clone())
                } else {
                    Delimiter::default()
                }
            }
            _ => Delimiter::Random(delimiter_list),
        }
    }
}

impl From<&str> for Delimiter {
    fn from(string: &str) -> Self {
        Delimiter::Character(string.to_string())
    }
}

impl From<String> for Delimiter {
    fn from(string: String) -> Self {
        Delimiter::Character(string)
    }
}

impl Delimiter {
    /// Get a string representation of a delimiter.
    ///
    /// The character variation of the delimiter with return the contained String.
    /// If it is the random variation a random item will be chosen from the vec.
    /// If it is RandomSpecialCharacter a random special character will be returned.
    pub(crate) fn to_string<R: ?Sized + Rng>(&self, rng: &mut R) -> String {
        match self {
            Delimiter::Character(s) => s.to_string(),
            // the vec can never be empty so unwrapping is fine here
            Delimiter::Random(l) => l.iter().choose(rng).unwrap().to_string(),
            // the special char array is static and will never be empty so unwrapping is fine here
            Delimiter::RandomSpecialCharacter => {
                SPECIAL_CHARS.iter().choose(rng).unwrap().to_string()
            }
        }
    }
}

/// Provides common functionality for different delimiters.
pub(crate) trait GetDelimiter {
    type Item;

    /// Gets a delimiter.
    fn to_string<R: ?Sized + Rng>(&self, rng: &mut R) -> Self::Item;
}

pub(crate) const SPECIAL_CHARS: &[&str] = &[
    "!", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">",
    "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~",
];

#[cfg(test)]
mod tests {
    use super::*;
    use rand::thread_rng;

    #[test]
    fn from_empty_vec() {
        let vec: Vec<String> = Vec::new();
        let delimiter = Delimiter::from(vec);
        assert!(delimiter == Delimiter::Character(" ".to_string()));
    }

    #[test]
    fn from_single_word_vec() {
        let word = "test".to_string();
        let vec: Vec<String> = vec![word.clone()];
        let delimiter = Delimiter::from(vec);
        assert!(delimiter == Delimiter::Character(word.clone()));
    }

    #[test]
    fn from_list_of_words() {
        let words: Vec<String> = vec!["test".to_string(), "foo".to_string(), "bar".to_string()];
        let delimiter = Delimiter::from(words.clone());
        assert!(delimiter == Delimiter::Random(words.clone()));
    }

    #[test]
    fn random_from_input() {
        let mut rng = thread_rng();
        let delims = vec!["!".to_string(), "@".to_string(), "*".to_string()];
        let delimiter = Delimiter::from(delims.clone());
        assert!(delims.contains(&delimiter.to_string(&mut rng)));
    }

    #[test]
    fn random_special_char() {
        let mut rng = thread_rng();
        let delimiter = Delimiter::RandomSpecialCharacter;
        assert!(SPECIAL_CHARS.contains(&delimiter.to_string(&mut rng).as_str()));
        assert!(SPECIAL_CHARS.contains(&delimiter.to_string(&mut rng).as_str()));
        assert!(SPECIAL_CHARS.contains(&delimiter.to_string(&mut rng).as_str()));
    }
}
