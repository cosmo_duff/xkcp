use dicewords::EffWordList;
use rand::prelude::SliceRandom;
use rand::Rng;
#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};
use std::iter::Iterator;
use std::str::FromStr;

impl WordList for EffWordList {
    fn get_random_words<R: Rng>(&self, rng: &mut R, count: usize) -> Vec<String> {
        self.choose_words(count, rng)
            .into_iter()
            .map(|s| s.into())
            .collect()
    }
}

/// Trait representing a word list that can be used to choose random words to be used to
/// generate a passphrase.
pub trait WordList {
    /// Returns a selcetion of random words form the word list.
    fn get_random_words<R: Rng>(&self, rng: &mut R, count: usize) -> Vec<String>;
}

impl WordList for Vec<String> {
    fn get_random_words<R: ?Sized + Rng>(&self, rng: &mut R, count: usize) -> Vec<String> {
        self.choose_multiple(rng, count)
            .map(|s| s.to_owned())
            .collect()
    }
}

impl WordList for &'static [&'static str] {
    fn get_random_words<R: ?Sized + Rng>(&self, rng: &mut R, count: usize) -> Vec<String> {
        self.choose_multiple(rng, count)
            .map(|s| s.to_string())
            .collect()
    }
}
