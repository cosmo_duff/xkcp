/*!
xkcp-core is a library that generates passwords based off of [XKCD 936](https://xkcd.com/936/).

## Usage

Currently this crate is not available on [crates.io](https://crates.io).  It can be used by adding
`xkcp-core` to the projects `Cargo.toml` and specifying the git repo.
```toml
[dependencies]
xkcp_core = { git = "https://gitlab.com/cosmo_duff/xkcp.git" }
```

## Documentation

Documentation can be built using the `cargo doc` command.

```bash
$ git clone https://gitlab.com/cosmo_duff/xkcp.git && cd xkcp/xkcp-core
$ cargo doc
# Open the doc in a web browser
$ cargo doc --open
```

## Example
```rust
// Imports:
//   PasswordGenerator
//   WordList (Trait)
//   Capitalization
//   Delimiter
use xkcp_core::prelude::*;
use dicewords::EffWordList;
use rand::thread_rng;

// Create a ThreadRng to perform the random selections. All functions to generate a password take
// a mutable object that implements the Rng trait.
let mut rng = thread_rng();

// Create the password generator object using the builder.
//
// The only required argument is an object that implements WordList.
let password_generator = PasswordGenerator::builder(EffWordList::LongWord)
    // Passwords will contain 5 random words.
    .count(5)
    // The words will be joined using a %.
    .delimiter(Delimiter::from("%"))
    // A random number from 0 to 1000 will be added to the password.
    .number(1000)
    // The first character in each word will be capitalized.
    .capitalization(Capitalization::Capitalize)
    .build();

// This function can not fail and will generate a single password using the parameters set in the
// builder.
let password = password_generator.generate_password(&mut rng);

// This function can not fail and will return a vec of 5 random passwords.
let passwords = password_generator.generate_passwords(&mut rng, 5);

// The failable version of the generate password function. It will attempt to generate a password
// within the set minimum and maximum lengths with the set number or retries. If a password cannot
// be generated within the constraints None is returned. The example below will try 100 times to
// generate a password with a maximum length of 16.
let try_password = password_generator.try_generate_password(&mut rng, 100, None, Some(16));

// The failable version of generate_passwords. It works as try_generate_password but will return a
// vec of Option<String>. Each time a password cannot be generated within the constraints None is
// returned. The example will try to generate 10 passwords with a maximum length of 20 and can
// retry each password 100 times.
let try_passwords = password_generator.try_generate_passwords(&mut rng, 10, 100, None, Some(20));
```


## Word Lists

The word lists come from the EFF and can be found
[here](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases). The lists are
licensed under the [CC (Creative Commons) 3.0](https://creativecommons.org/licenses/by/3.0/us/).
The library uses the [long](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt) and
[short](https://www.eff.org/files/2016/09/08/eff_short_wordlist_1.txt) lists.
*/

mod capitalization;
mod delimiter;
mod generate_password;
mod generator_builder;
mod password_generator;
mod wordlist;

/// Prelude for xkcp-core.
///
/// Imports all needed structs and enums for creating and using a PasswordGenerator.
pub mod prelude {
    pub use crate::capitalization::Capitalization;
    pub use crate::delimiter::Delimiter;
    pub use crate::generate_password::GeneratePassword;
    pub use crate::password_generator::PasswordGenerator;
    pub use crate::wordlist::WordList;
    pub use dicewords::EffWordList;
}

pub use capitalization::Capitalization;
pub use delimiter::Delimiter;
pub use dicewords::EffWordList;
pub use generate_password::GeneratePassword;
pub use generator_builder::GeneratorBuilder;
pub use password_generator::PasswordGenerator;
pub use wordlist::WordList;

#[cfg(test)]
mod tests {
    use crate::generate_password;

    use super::delimiter::SPECIAL_CHARS;
    use super::prelude::*;
    use rand::thread_rng;

    fn check_password<W>(settings: &PasswordGenerator<W>, password: &str) {
        let mut rng = thread_rng();
        assert!(password.contains(&settings.delimiter.to_string(&mut rng)));
        let parts: Vec<&str> = password
            .split(&settings.delimiter.to_string(&mut rng))
            .collect();
        if let Some(x) = settings.number {
            assert!(parts.len() == settings.count + 1);
            if let Some(n) = parts.last() {
                let number = n.parse::<u64>().unwrap();
                assert!(number > 0 && number <= x);
            }
        } else {
            assert!(parts.len() == settings.count);
        }
        let end = match settings.number {
            Some(_) => parts.len() - 1,
            None => parts.len(),
        };
        for word in &parts[..=end] {
            assert!(word.len() > 0);
        }
    }

    #[test]
    fn test_generate_password() {
        let mut rng = thread_rng();
        let password = PasswordGenerator::builder(EffWordList::LongWord)
            .count(5)
            .delimiter(Delimiter::from("%"))
            .number(1000)
            .capitalization(Capitalization::Capitalize)
            .build();
        let gen_pass = password.generate_password(&mut rng);
        check_password(&password, &gen_pass);
    }

    #[test]
    fn test_try_generate_password() {
        let mut rng = thread_rng();
        let password = PasswordGenerator::builder(EffWordList::ShortWord1)
            .count(2)
            .delimiter(Delimiter::from("!"))
            .number(1000)
            .capitalization(Capitalization::Upper)
            .build();
        let gen_pass = password.try_generate_password(&mut rng, 100, None, Some(16));
        if let Some(pass) = gen_pass {
            assert!(pass.len() <= 16);
            check_password(&password, &pass);
        }
    }

    #[test]
    fn test_generate_passwords() {
        let mut rng = thread_rng();
        let password = PasswordGenerator::builder(EffWordList::LongWord)
            .count(5)
            .delimiter(Delimiter::from("^"))
            .number(1000)
            .capitalization(Capitalization::Capitalize)
            .build();
        let passwords = password.generate_passwords(&mut rng, 5);
        assert!(passwords.len() == 5);
        for p in passwords {
            check_password(&password, &p);
        }
    }

    #[test]
    fn test_try_generate_passwords() {
        let mut rng = thread_rng();
        let password = PasswordGenerator::builder(EffWordList::LongWord)
            .count(5)
            .delimiter(Delimiter::from("^"))
            .number(1000)
            .capitalization(Capitalization::Capitalize)
            .build();
        let passwords = password.try_generate_passwords(&mut rng, 10, 100, None, Some(20));
        assert!(passwords.len() == 10);
        for p in passwords {
            if let Some(gen_pass) = p {
                check_password(&password, &gen_pass);
            }
        }
    }

    #[test]
    fn test_multiple_delimiters() {
        let mut rng = thread_rng();
        let generator = PasswordGenerator::builder(EffWordList::LongWord)
            .count(10)
            .delimiter(Delimiter::RandomSpecialCharacter)
            .multiple_delimiters(true)
            .capitalization(Capitalization::Capitalize)
            .build();

        let password = generator.generate_password(&mut rng);
        // Create a list of special chars in the password
        let mut special_chars: Vec<_> = password
            .chars()
            .filter_map(|c| {
                let mut b = [0; 8];
                let encoded = &c.encode_utf8(&mut b).to_owned();
                if SPECIAL_CHARS.contains(&encoded.as_str()) {
                    Some(c)
                } else {
                    None
                }
            })
            .collect();

        // After removing duplicates from the used special characters we should end up with more
        // than one special character
        special_chars.sort();
        special_chars.dedup();
        assert!(special_chars.len() > 1);
    }

    #[test]
    fn test_random_number_placement() {
        let mut rng = thread_rng();
        let generator = PasswordGenerator::builder(EffWordList::LongWord)
            .count(10)
            .delimiter(Delimiter::RandomSpecialCharacter)
            .number(1000)
            .random_number_placement(true)
            .multiple_delimiters(true)
            .capitalization(Capitalization::Capitalize)
            .build();

        let passwords = generator.generate_passwords(&mut rng, 10);
        let mut first_pos: Vec<usize> = Vec::with_capacity(passwords.len());
        for password in passwords {
            for (index, c) in password.chars().enumerate() {
                if c.is_numeric() {
                    first_pos.push(index);
                }
            }
        }

        first_pos.sort();
        first_pos.dedup();

        assert!(first_pos.len() > 1);
    }
}
