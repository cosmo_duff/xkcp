use std::str::FromStr;

use rand::seq::SliceRandom;
#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};

#[derive(Debug, Clone, Eq, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
/// The capitalization scheme to use when generating the password.
pub enum Capitalization {
    /// All characters will be uppercase.
    Upper,
    /// All characters will be lowercase.
    Lower,
    /// The first letter of each word will be capitalized.
    Capitalize,
    /// Picks a random capitalization scheme for each word. [Upper, Lower, Capitalize]
    Random,
}

impl FromStr for Capitalization {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lower = s.to_lowercase();
        match lower.as_str() {
            "upper" => Ok(Capitalization::Upper),
            "lower" => Ok(Capitalization::Lower),
            "capitalize" => Ok(Capitalization::Capitalize),
            "random" => Ok(Capitalization::Random),
            _ => Err("Unknown format"),
        }
    }
}

impl Capitalization {
    pub(crate) fn apply_scheme(&self, word: &mut String) {
        match *self {
            Capitalization::Upper => {
                *word = word.to_uppercase();
            }
            Capitalization::Lower => {
                *word = word.to_lowercase();
            }
            Capitalization::Capitalize => {
                if let Some(w) = title_capitalize(word) {
                    *word = w;
                };
            }
            Capitalization::Random => {
                const CHOICES: &[&str] = &["upper", "lower", "capitalize"];
                let mut rng = rand::thread_rng();
                // this should not really fail since we are pulling from a list of vaild str values
                // that Capitalization can be created from but if it does we will use the default
                let scheme_str = CHOICES.choose(&mut rng).unwrap_or(&"lower");
                let scheme: Capitalization =
                    Capitalization::from_str(scheme_str).unwrap_or_default();
                scheme.apply_scheme(word);
            }
        }
    }
}

impl Default for Capitalization {
    fn default() -> Self {
        Capitalization::Lower
    }
}

fn title_capitalize(word: &mut String) -> Option<String> {
    let s = word.get_mut(0..1);
    if let Some(s) = s {
        s.make_ascii_uppercase()
    } else {
        return None;
    }
    Some(word.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;
    // TO DO: Figure out how to test random or what it is actually testing

    #[test]
    fn test_upper() {
        let scheme = Capitalization::Upper;
        let mut word = "Testerdoodledoo".to_string();
        scheme.apply_scheme(&mut word);
        assert!(&word == "TESTERDOODLEDOO");
    }

    #[test]
    fn test_lower() {
        let scheme = Capitalization::Lower;
        let mut word = "tESTERdoodlEDOO".to_string();
        scheme.apply_scheme(&mut word);
        assert!(&word == "testerdoodledoo");
    }

    #[test]
    fn test_capitalize() {
        let scheme = Capitalization::Capitalize;
        let mut word = "thetshouldendupuppercase".to_string();
        scheme.apply_scheme(&mut word);
        assert!(word.starts_with("T"));
    }
}
